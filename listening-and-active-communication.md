# Listening and Active Communication

### Q1. What are the steps/strategies to do Active Listening?

* Concentrating on the speaker and the topic rather than being distracted by one's thoughts.
* Letting the other person finish their thought without interfering
* Showing that you are listening with body language
* Taking notes during important conversations
* Paraphrase what others have said to make sure you are following them.


### Q2. According to Fisher's model, what are the key points of Reflective Listening?

The key points of reflective listening according to Fisher's model are as follows:

* We must concentrate on what the other person is saying without interrupting.
* We have to encourage each other to speak freely. By embracing the speaker's perspective
* We must quiet our minds and focus completely on the speaker's mood, reflecting his emotional state through words and non-verbal communication.
* We can summarise what the speaker said, using his own words.
* We have to respond to the speaker's specific point without deviating from other subjects.
* If required, we can switch the roles of speaker and listener.

### Q3: What are the obstacles in your listening process?

* Mental churning
* If it is a new subject, not be able to grasp it quickly.
* Being in a hurry and having a nervous disposition

### Q4: What can you do to improve your listening?

* Focusing one at a time.
* Practicing yoga and meditation.
* Try to stick to the schedule.

### Q5: When do you switch to Passive communication style in your day to day life?

* When feeling lonely and sad
* When I committed mistakes
* When I am completely in a new place with unknown people.

### Q6: When do you switch into Aggressive communication styles in your day to day life?

* When got frustrated 
* When lost the patience 
* When repeatedly bullying me

### Q7: When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

* When I am hurt to the core and not able to forget the insulting situation
  
### Q8: How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? 

* By clarifying what I know and what I need to know
* By planning ahead of time and attempting to execute them flawlessly.
* By always remembering that time is valuable and that once an opportunity is gone, it is gone forever. 