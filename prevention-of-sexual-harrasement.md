# Prevention of Sexual Harassment 

### Q. What kinds of behavior cause sexual harassment?

Sexual harassment can happen in 3 ways that are unwelcome behavior in nature. These can affect the work environment very badly.

* Verbal 
* Visual 
* Physical

### Verbal Harassment:
These include 
* Comments about clothing
* Comments about a person's body
* Sexual or gender-based jokes or remarks
* Repeatedly asking out a person Spreading rumors
* Using foul and abusive language

### Visual Harassment :
These include the sexual nature of 
* Posters
* Drawing pictures
* Cartoons
* Emails

### Physical Harassment :
These include 
* Touching the person's body, clothing
* Hugging, kissing, patting, or stroking
* Touching or rubbing oneself 
* Sexual gesturing

These can be categorized into two types:
1. Quid pro quo harassment: this occurs when a supervisor's request for sexual favors or other sexual conduct results in a tangible job action. 
2. Hostile work environment: this occurs when an employee is subjected to unwelcome physical or verbal conduct of a sexual nature 

### Q. What would you do in case you face or witness any incident or repeated incidents of such behavior?

Immediately we can tell them to stop the harassment. If that does not happen we can tell the supervisor and higher authority who can act on it and make sure harassing stops.

While reporting you can document everything. The following steps can be taken while documenting the case.

 Write down details such as:

1. The date, time, and location of the harassment, what happened, what was said, and who witnessed the behavior. 

1. Keep copies or take screenshots of any relevant emails, texts, photos, or social posts.

2. Tell a trusted friend, family member, or co-worker what happened, and write down the details of those conversations.

3. Store all documentation outside your office or your work computer and make sure it’s backed up in a safe place. 

This can be reported to the concerned officials with the appropriate proof. 
